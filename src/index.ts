import path from 'path';
import { logger } from './framework/logging';

import { Bot } from './framework/bot';
import { CommandLoader } from './framework/loaders/commandLoader';

// The bot token is either passed in via a .env file (preferred)
// *or* alternatively, via the command line by setting TOKEN=<bot token>.
const token = process.env.TOKEN;
const client = new Bot('mi!');

client.commandLoader.loadCommands(client, path.join(__dirname, '/commands'));
client.hookLoader.loadHooks(client, path.join(__dirname, '/hooks'));

// Begin the login process
logger.info('calling client.login()');
client.authenticate(token);