import { Bot } from '../framework/bot';
import { Message } from 'discord.js';

import { EventHandler } from '../framework/event';
import { CommandHandler } from './commandHandler';
import { logger } from '../framework/logging';

/**
 * Message event handler for the bot
 */
export class MessageHandler extends EventHandler {
    commandHandler: CommandHandler;

    constructor() {
        // Register a command handler here
        super();

        this.commandHandler = new CommandHandler();
    }
    
    execute(client: Bot, msg: Message): void {
        logger.info(`> ${msg.author.username}#${msg.author.discriminator}: ${msg.content}`);

        if (msg.author.bot) 
            return;

        // Execute message hooks
        for (const [hookName, hook] of client.hookLoader.hooks) {
            if (hook.properties.trigger == 'message' && hook.properties.isEnabled) {
                // Execute the hook
                hook.executeHook(client, msg, []);
            }
        }

        if (msg.content.startsWith(client.prefix)) {
            this.commandHandler.execute(client, msg);
        }
    }
}