import { Bot } from '../framework/bot';
import { GuildMember, Message } from 'discord.js';
import { EventHandler } from '../framework/event';
import { Command } from '../framework/command';

import { logger } from '../framework/logging';
import { errorEmbed } from '../embedUtil';

/**
 * Command handler for the bot. Executes commands registered when a message
 * starting with the prefix (by default: mi!) is encountered.
 */
export class CommandHandler extends EventHandler {
    constructor() {
        super();
    }

    /**
     * Checks whether the member has permission to use an owner-only command.
     * Returns `true` if the user does, `false` if they don't
     * @param command
     * @param member 
     */
    isOwner(command: Command, member: GuildMember): boolean {
        if (command.properties.ownerOnly) {
            // TODO: store in .env file?
            if (member.id == '481284371312279573') {
                return true;
            }
            return false;
        }

        // Return true if the command is not owner only
        return true;
    }

    /**
     * Checks whether the user has the required permissions (all of them) to use
     * a command.
     * 
     * Returns `true` if the user does, `false` if they don't
     * @param command 
     * @param member 
     */
    hasPermissions(command: Command, member: GuildMember): boolean {
        if (command.properties.requiredPermissions !== undefined) {
            // Check permissions of the member
            let permissionCount = 0;

            for (const permission of command.properties.requiredPermissions) {
                if (member?.hasPermission(permission)) {
                    permissionCount++;
                }
            }

            if (permissionCount >= command.properties.requiredPermissions.length) {
                // All permissions were met
                return true;
            }

            // Failed to meet required permissions
            return false;
        }
        else {
            // No permissions are required
            return true;
        }
    }

    execute(client: Bot, msg: Message): void {
        const args = msg.content.slice(client.prefix.length).trim().split(/ +/);
        const commandName = args.shift()!.toLowerCase()

        const commands = client.commandLoader.commands;

        // TODO: support command aliases
        if (!commands.has(commandName)) {
            logger.error(`command not found: ${commandName}`);

            const reply = errorEmbed(`Command not found: ${commandName}`);
            msg.channel.send(reply);

            // return to prevent another error triggering
            return;
        }

        // Find and execute the command
        try {
            const command = commands.get(commandName)!;
            const commandArgs = msg.content.split(' ').splice(1);
            
            if (!command.properties.ownerOnly) {
                // Check for permissions
                if (this.hasPermissions(command, msg.member!)) {
                    command.execute(client, msg, commandArgs);
                }
                else {
                    const reply = errorEmbed(`You don't have the required permissions (${command.properties.requiredPermissions})!`);
                    msg.channel.send(reply);
                }
            }
            else if (this.isOwner(command, msg.member!)) {
                command.execute(client, msg, commandArgs);
            }
            else {
                const reply = errorEmbed('Sorry but only bot owners can use that command!');
                msg.channel.send(reply);
            }
        } catch (err) {
            logger.error(err)
            const reply = errorEmbed(err);
            msg.channel.send(reply);

            // return to prevent another error triggering
            return;
        }
    }
}