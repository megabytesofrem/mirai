import { Collection, Message } from "discord.js";
import { Command } from "../command";
import { logger } from '../logging';

import { join } from 'path';
import { readdirSync, statSync } from 'fs';
import { Bot } from "../bot";
//import { getCommandsInGroup } from "../util";

export class CommandLoader {
    commands: Collection<string, Command>;
    paths: string[];

    constructor() {
        this.commands = new Collection<string, Command>();
        this.paths = [];
    }
    
    private walkDirectory(directory: string): string[] {
        // Walk a directory recursively

        const items = readdirSync(directory);
        for (const item of items) {
            const path = join(directory, item);

            if (path.endsWith('.ts')) {                    
                // Found a command, push it to the list
                this.paths.push(path.replace(/\.ts/, ''));
            }

            const stats = statSync(path);
            if (stats.isDirectory())
                this.walkDirectory(path);
        }

        return this.paths;
    }

    async loadCommands(client: Bot, directory: string): Promise<void> {
        const filePaths = this.walkDirectory(directory);

        for (const path of filePaths) {
            let commandName;

            try {
                const commandClass = await import(`${path}`);
                const command: Command = new commandClass.default();

                // Determine the command name based on the command class
                // instead of based on the file name. This allows command files to be named
                // differently *and* most importantly, alias support is now possible!

                commandName = command.properties.name;

                // Load the command
                logger.info(`loading command ${commandName} (${path})`);

                // Handle any command aliases
                if (command.properties.aliases !== undefined) {
                    for (const alias of command.properties.aliases) {
                        logger.info(`registered alias ${alias} for ${commandName}`);

                        this.commands.set(alias, command);
                    }
                }
                
                this.commands.set(commandName, command);
                logger.info(`loaded command ${commandName} successfully`);
            } catch (err) {
                logger.error(`failed to load command "${commandName}"`);
                logger.error(err);
            }
        }
    }

    async unloadModule(): Promise<void> {
        // TODO: Add module loading/unloading
        /* stub */
    }
}