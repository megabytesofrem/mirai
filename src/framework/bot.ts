import { Client } from 'discord.js'
import { Collection } from 'discord.js'

// Handlers
import { MessageHandler } from '../listeners/messageHandler';
import { logger } from '../framework/logging';

import { EventHandler } from './event';
import { CommandLoader } from './loaders/commandLoader';
import { HookLoader } from './loaders/hookLoader';
import { ReactionHandler } from '../listeners/reactionHandler';

export class Bot extends Client {
    prefix: string;
    commandLoader: CommandLoader;
    hookLoader: HookLoader;
    handlers: Collection<string, EventHandler>;

    constructor(prefix: string) {
        super({
            disableMentions: 'everyone', 
        });

        this.prefix = prefix;
        this.commandLoader = new CommandLoader();
        this.hookLoader = new HookLoader();
        this.handlers = new Collection();
    }

    /**
     * Authenticate with the Discord API and register default handlers
     * @param token discord auth token, either from .env or TOKEN=
     */
    authenticate(token: string | undefined): void {
        super.login(token);

        // Register message handler
        this.handlers.set('message', new MessageHandler());
        this.handlers.set('messageReactionAdd', new ReactionHandler());

        for (const key of this.handlers.keys()) {
            const handler = this.handlers.get(key)!;

            logger.info(`registered event handler for "${key}"`);

            if (key != 'messageReactionAdd') {
                this.on(key, (msg, a) => handler.execute(this, msg, a));
            }
            else {
                this.on(key, (reaction, user) => handler.execute(this, user, reaction, []));
            }
        }
    }
}