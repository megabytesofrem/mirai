/**
 * Base EventHandler class that other event handlers should
 * derive from.
 */
import { Message, MessageReaction, PartialUser, User } from 'discord.js';
import { Bot } from './bot';

export class EventHandler {
    /**
     * Execute the event handler. This function will be called
     * when the event handler is fired, e.g for a MessageHandler
     * this will be called when the client recieves a new message.
     * 
     * @param {*} client
     * @param {*} msg
     * @param {*} args
     */
    // eslint-disable-next-line
    execute(client: Bot, msg?: Message | User | PartialUser, reaction?: MessageReaction, args?: Array<unknown>) {}
}