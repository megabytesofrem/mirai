import { createLogger, format, transports } from 'winston';
const { combine, colorize, timestamp, printf } = format;

const customFormat = printf(({ level, message, timestamp}) => {
    return `[${timestamp}][${level}]: ${message}`;
});

// Static logger with custom formatting defined above
export const logger = createLogger({
    format: combine(
        colorize(),
        timestamp({
            format: 'shortTime'
        }),
        customFormat,
    ),
    transports: [new transports.Console()]
});