/**
 * Very basic wrapper around the Reddit API.
 * Stolen from https://github.com/chxrlt/mirai-bot/blob/master/src/commands/fun/eggirl.js
 */

import { Channel, sendThenReact } from '../util';

import axios from 'axios';
import _ from 'lodash';
import { logger } from '../framework/logging';

// Whether the filter by the X hottest or newest posts
type RedditFilter = 'hot' | 'new';

/**
 * Send a random post from a subreddit
 * @param channel the channel to send in
 * @param subreddit the subreddit name
 * @param filter the algorithm to filter posts by
 * @param titles an array of titles to filter posts by
 * @param limit the limit of posts to fetch
 */
export function sendRandomPost(
    channel: Channel, 
    subreddit: string, 
    filter: RedditFilter,
    titles?: string[],
    limit?: number): void {
    axios.get(`https://www.reddit.com/r/${subreddit}/${filter}.json?limit=${limit}`)
        .then(response => {
            const stuff = response.data['data']['children'];
    
            for (const thing of stuff) {
                if (thing['kind'] == 't3') {
                
                    // Check for the title matching the titles array
                    if (titles !== undefined) {
                        for (const c of titles) {
                            if (thing['data']['title'].includes(c)) {
                                return sendThenReact(channel, _.sample(stuff)['data']['url'], '✅');
                            }
                        }
                    }
                    else {
                        // Don't filter by title, just return the post
                        return sendThenReact(channel, _.sample(stuff)['data']['url'], '✅');
                    }
                }
            }
        })
       .catch(err => logger.error(err));
    }