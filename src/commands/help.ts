import { Collection, Message, MessageEmbed } from "discord.js";
import { Bot } from "../framework/bot";
import { Command } from "../framework/command";

import { Colors, errorEmbed, wrapInBlock } from '../embedUtil';
import { getCommandsInGroup } from "../util";

export default class HelpCommand extends Command {
    groups: string[] = ['default', 'fun', 'moderation'];

    constructor() {
        super({
            name: 'help',
            group: 'default',
            description: 'displays help for the bot',
            ownerOnly: false
        });
    }

    private displayCommandsList(client: Bot, msg: Message) {
        // Display a list of all commands

        const keyTypes = {
            user: '-',      // user command (aka no permissions required)
            admin: '+',     // admin only command (KICK_MEMBERS, BAN_MEMBERS)
            ownerOnly: '*'  // owner only command
        };

        const embed = new MessageEmbed()
            .setTitle(`Commands for Mirai`)
            .setDescription('Below are a list of all commands and their groups!')
            .setColor(Colors.normal)
            
        for (const group of this.groups) {
            let groupDescription = '';

            // Loop through each group and get a list of its commands
            const commandsInGroup = getCommandsInGroup(client, group);
            
            for (const key of commandsInGroup.keys()) {
                const command = commandsInGroup.get(key);

                // Filter out any aliased commands
                if (command?.properties.aliases?.includes(key)) {
                    continue;
                }

                let type = keyTypes.user;

                if (command?.properties.requiredPermissions?.includes('KICK_MEMBERS') ||
                    command?.properties.requiredPermissions?.includes('BAN_MEMBERS')) {
                    type = keyTypes.admin;
                }
                else if (command?.properties.ownerOnly) {
                    type = keyTypes.ownerOnly;
                }

                groupDescription += `${type} ${key}\n`
            }

            // Add a field to the embed for each group
            embed.addField(`❯ ${group}`, wrapInBlock(groupDescription));
        }

        msg.channel.send(embed);
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        if (args.length == 0) {
            this.displayCommandsList(client, msg);
        }

        if (args.length == 1) {
            const commandName = args[0];
            const command = client.commandLoader.commands.get(commandName);

            if (command === undefined) {
                const reply = errorEmbed(`Command not found: ${commandName}`);
                msg.channel.send(reply);

                return;
            }

            console.log(command?.properties)

            const embed = new MessageEmbed()
                .setTitle(`${commandName}`)
                .addField('❯ Description', `\`${command?.properties.description}\``)
                .setColor(Colors.normal)

            embed.addField('❯ Group', command?.properties.group);

            if (command?.properties.requiredPermissions && command?.properties.requiredPermissions.length > 0) {
                const permissions = command?.properties.requiredPermissions;
                embed.addField('❯ Required Permissions', permissions);
            }

            // List any optional aliases
            if (command.properties.aliases)
               embed.addField('❯ Aliases', command.properties.aliases.map(alias => `\`${alias}\``).join(','));

            // ... and display the usage, if there is one
            if (command.properties.usage)
               embed.addField('❯ Usage', `\`mi!${command.properties.usage}\``);

            msg.channel.send(embed);
        }
    }
}

