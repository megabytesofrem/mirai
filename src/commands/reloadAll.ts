import { Message } from "discord.js";
import { Bot } from "../framework/bot";
import path from 'path';
import { Command } from "../framework/command";

export default class ReloadAllCommand extends Command {
    constructor() {
        super({
            name: 'reloadall',
            aliases: ['reload'],
            group: 'default',
            description: 'reload all modules/commands',
            ownerOnly: true
        });
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        // Clear out the commands to prevent caching
        client.commandLoader.commands.clear();

        client.commandLoader.loadCommands(client, __dirname);
        msg.reply(`success! Reloaded all commands`);
    }
}