import { TextChannel, Message, DMChannel, NewsChannel } from "discord.js";
import { errorEmbed, wrapInBlock } from "../../embedUtil";
import { Bot } from "../../framework/bot";
import { Command } from "../../framework/command";

// Reddit API helpers
import { sendRandomPost } from '../../apis/reddit';

export default class EggirlCommand extends Command {
    constructor() {
        super({
            name: 'eggirl',
            group: 'fun',
            description: 'replies with a eggirl post',
            ownerOnly: false
        });
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        sendRandomPost(msg.channel, 'egg_irl', 'hot', ['egg', 'irl'], 200);
    }
}