import { Message } from "discord.js";
import { errorEmbed } from "../../embedUtil";
import { Bot } from "../../framework/bot";
import { Command } from "../../framework/command";

// Reddit API helpers
import { sendRandomPost } from '../../apis/reddit';

export default class RedditCommand extends Command {
    constructor() {
        super({
            name: 'reddit',
            aliases: ['r'],
            group: 'fun',
            usage: 'reddit <subreddit>',
            description: 'replies with a random reddit post',
            ownerOnly: false
        });
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        if (args.length < 1) {
            const reply = errorEmbed('Please specify a subreddit!');
            msg.channel.send(reply);

            return;
        }

        const subreddit = args[0];

        msg.channel.send(`:thinking: Fetching a random post from \`r/${subreddit}\``)
        sendRandomPost(msg.channel, subreddit, 'hot', undefined, 200)
    }
}