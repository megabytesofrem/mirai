import { TextChannel, Message, DMChannel, NewsChannel } from "discord.js";
import { errorEmbed, wrapInBlock } from "../../embedUtil";
import { Bot } from "../../framework/bot";
import { Command } from "../../framework/command";
import _ from 'lodash';

// "import" the JSON file containing the copypastas content
import pastas from '../../static/pastas.json';

type Channel = TextChannel | DMChannel | NewsChannel;

export default class PastaCommand extends Command {
    constructor() {
        super({
            name: 'pasta',
            aliases: ['cp', 'cpasta'],
            group: 'fun',
            usage: 'pasta [illegal|gnu|ytcomment|random]',
            description: 'replies with a copypasta',
            ownerOnly: false
        });
    }

    private sendPasta(channel: Channel, pasta: string) {
        let invalidPasta = false;
        let pastaContentArray: string[] = [];
        let messageContent = '';
        const invalidPastaError = errorEmbed('Invalid copypasta specified!');

        // Valid copy pasta, send the copypasta
        switch (pasta) {
            case 'illegal':
                pastaContentArray = pastas.strings.illegal;
                break;
            case 'gnu': case 'gnu/linux':
                pastaContentArray = pastas.strings.gnu;
                break;
            case 'ytcomment': case 'youtubecomment':
                pastaContentArray = pastas.strings.ytcomment;
                break;
            default:
                // Invalid copypasta
                channel.send(invalidPastaError);
                invalidPasta = true;
                break;
        }

        if (!invalidPasta) {
            for (const line of pastaContentArray) {
                messageContent += line;
            }

            channel.send(wrapInBlock(messageContent));
        }
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        if (args.length == 1) {
            const pasta = args[0];

            if (pasta != 'random') {
                this.sendPasta(msg.channel, pasta);
            }
            else {
                // Fetch a random pasta
                msg.channel.send(':game_die: Fetching a random copypasta');
                const pastaNames = ['illegal', 'gnu', 'ytcomment'];
                const randomPasta = _.sample(pastaNames)!;

                this.sendPasta(msg.channel, randomPasta);
            }
        }
    }
}