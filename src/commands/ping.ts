import { Message } from "discord.js";
import { Bot } from "../framework/bot";
import { Command } from "../framework/command";

export default class PingCommand extends Command {
    constructor() {
        super({
            name: 'ping',
            group: 'default',
            description: 'replies pong',
            ownerOnly: false
        });
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        msg.reply('pong!');
    }
}