import { Message } from "discord.js";
import { Bot } from "../../framework/bot";
import { Command } from "../../framework/command";

import { findMember, getDiscordTag } from '../../util';
import { errorEmbed } from '../../embedUtil';

export default class BanCommand extends Command {
    constructor() {
        super({
            name: 'ban',
            group: 'moderation',
            usage: 'ban <user> [reason]',
            description: 'bans a user lol',
            requiredPermissions: [
                'BAN_MEMBERS'
            ],
            ownerOnly: false
        });
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        if (args.length < 1) {
            const reply = errorEmbed('Please specify who to ban!');
            msg.channel.send(reply);

            return;
        }

        if (args.length >= 1) {
            // Extract the user id
            const targetMember = await findMember(client, msg, args[0]);
            const banReason = args[1] !== undefined ? args[1] : 'No reason specified';

            try {
                const user = getDiscordTag(targetMember);

                if (targetMember?.bannable) {
                    // Try to ban the member
                    targetMember.ban();
                    msg.channel.send(`Banned ${user} for ${banReason}`);
                }
                else {
                    const reply = errorEmbed(`Failed to ban, ${user} is not bannable. Are they above me?`);
                    msg.channel.send(reply);
                }
            } catch (err) {
                const reply = errorEmbed(`Failed to ban: ${err}`);
                msg.channel.send(reply);
            }
        }
    }
}