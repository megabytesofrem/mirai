import { Message } from "discord.js";
import { Bot } from "../../framework/bot";
import { Command } from "../../framework/command";

import { findMember, getDiscordTag } from '../../util';
import { errorEmbed } from '../../embedUtil';

export default class KickCommand extends Command {
    constructor() {
        super({
            name: 'kick',
            aliases: ['yeet'],
            group: 'moderation',
            usage: 'kick <user> [reason]',
            description: 'kicks a user lol',
            requiredPermissions: [
                'KICK_MEMBERS'
            ],
            ownerOnly: false
        });
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        if (args.length < 1) {
            const reply = errorEmbed('Please specify who to kick!');
            msg.channel.send(reply);

            return;
        }

        if (args.length >= 1) {
            // Extract the user id
            const targetMember = await findMember(client, msg, args[0]);
            const kickReason = args[1] !== undefined ? args[1] : 'No reason specified';

            try {
                const user = getDiscordTag(targetMember);

                if (targetMember?.kickable) {
                    // Try to kick the member
                    targetMember.kick(kickReason);
                }
                else {
                    const reply = errorEmbed(`Failed to kick, ${user} is not kickable. Are they above me?`);
                    msg.channel.send(reply);
                }
            } catch (err) {
                const reply = errorEmbed(`Failed to kick: ${err}`);
                msg.channel.send(reply);
            }
        }
    }
}