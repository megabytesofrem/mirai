import { Message } from "discord.js";
import { Bot } from "../framework/bot";
import { Command } from "../framework/command";

import { sendThenReact } from '../util';
import { errorEmbed } from '../embedUtil';

export default class SelfRoleCommand extends Command {
    // TODO: make this configurable per server        
    blacklistedRoles = ['princess ♡', 'prince ♡', 'admin', 'moderator', 'cis bots'];

    constructor() {
        super({
            name: 'selfrole',
            group: 'default',
            usage: 'selfrole [add|rm] <role name>',
            description: 'assigns a role to yourself',
            ownerOnly: false
        });
    }

    private addRole(msg: Message, roleName: string) {
        try {
            const targetRole = msg.guild?.roles.cache.find(role => role.name == roleName);
            msg.member?.roles.add(targetRole!);

            if (targetRole === undefined) {
                throw new Error(`tried to find ${roleName}, but got undefined`);
            }
            sendThenReact(msg.channel, `Added role ${targetRole}!`, '✅');
        } catch (err) {
            const reply = errorEmbed(`Failed to add role: ${err}`);
            msg.channel.send(reply);
        }
    }

    private removeRole(msg: Message, roleName: string) {
        try {
            const targetRole = msg.guild?.roles.cache.find(role => role.name == roleName);
            msg.member?.roles.remove(targetRole!);

            if (targetRole === undefined) {
                throw new Error(`tried to find ${roleName}, but got undefined`);
            }
            sendThenReact(msg.channel, `Removed role ${targetRole}!`, '✅');
        } catch (err) {
            const reply = errorEmbed(`Failed to remove role: ${err}`);
            msg.channel.send(reply);
        }
    }

    async execute(client: Bot, msg: Message, args: Array<string>): Promise<void> {
        const mode = args[0];

        if (args.length < 1) {
            const reply = errorEmbed('Please specify a role to assign');
            msg.channel.send(reply);

            return;
        }

        if (args.length >= 1) {
            const roleName = args.slice(1).join(' ');

            // Check if the role is blacklisted
            if (this.blacklistedRoles.includes(roleName)) {
                const reply = errorEmbed('That role is blacklisted!');
                msg.channel.send(reply);
    
                return;
            }
    
            if (mode == 'add') {
                this.addRole(msg, roleName);
            }
            else if (mode == 'rm') {
                this.removeRole(msg, roleName);
            }
        }
    }
}