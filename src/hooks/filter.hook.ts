import { Message, User } from "discord.js";
import { Bot } from "../framework/bot";
import { Hook } from "../framework/hook";

import filter from '../static/filter.json';

export default class FilterHook extends Hook {
    shouldBlockNSFW: boolean;
    blacklist: string[];  

    constructor() {
        /* stub */
        super({
            name: 'filterHook',
            isEnabled: true,
            trigger: 'message'
        });

        this.shouldBlockNSFW = false;

        // Load NSFW sites into the blacklist
        this.blacklist = filter.nsfw.sites;
    }

    /** Checks whether the string contains anything in the blacklist */
    checkMessage(s: string, blacklist: string[] = this.blacklist): boolean {
        for (const item of blacklist) {
            if (s.includes(item))
                return true;
        }
        return false;
    }

    // eslint-disable-next-line
    async executeHook(client: Bot, msg: Message, user: User): Promise<void> {
        // Block NSFW content
        if (filter.nsfw.blocked_channels.includes(msg.channel.id)) {
            if (this.shouldBlockNSFW) {
                if (this.checkMessage(msg.content)) {
                    msg.reply('https://i.kym-cdn.com/entries/icons/original/000/033/758/Screen_Shot_2020-04-28_at_12.21.48_PM.png');
                    msg.delete();
                }
            }
        }
        else {
            // Block words
            if (this.checkMessage(msg.content, filter.words)) {
                msg.reply('no.');
                msg.delete();
            }
        }
    }
}