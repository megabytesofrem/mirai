import { Message, User } from "discord.js";
import { Bot } from "../framework/bot";
import { Hook } from "../framework/hook";

export default class StarboardHook extends Hook {
    // TODO: make this configurable per server        

    constructor() {
        /* stub */
        super({
            name: 'starboardHook',
            isEnabled: true,
            trigger: 'message'
        });
    }

    // eslint-disable-next-line
    async executeHook(client: Bot, msg: Message, user: User): Promise<void> {
    }
}