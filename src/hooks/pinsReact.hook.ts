import { Message, MessageEmbed, MessageReaction, TextChannel, User } from "discord.js";
import { Bot } from "../framework/bot";
// import { Command } from "../framework/command";

import { sendThenReact } from '../util';
import { Colors, errorEmbed, wrapInBlock } from '../embedUtil';
import { logger } from "../framework/logging";
import { Hook } from "../framework/hook";

export default class PinsReactHook extends Hook {
    // TODO: make this configurable per server      
    pinsChannel = '770669609312321587';

    constructor() {
        /* stub */
        super({
            name: 'pinsReactHook',
            isEnabled: true,
            trigger: 'reaction'
        });
    }

    // eslint-disable-next-line
    async executeHook(client: Bot, msg: Message, user: User, reaction: MessageReaction): Promise<void> {
        if (user.bot)
            return;

        if (reaction.emoji.name !== '📌')
            return;

        const channel = client.channels.cache.get(this.pinsChannel);
        if (channel instanceof TextChannel) {
            const messageChannel = <TextChannel> channel;

            const embed = new MessageEmbed()
                .setTitle('Pin')
                .setDescription(wrapInBlock(msg.content))
                .addField('Author', `${msg.author.username}#${msg.author.discriminator}`)
                .addField('Date', msg.createdAt)
                .setColor(Colors.normal);

            messageChannel.send(embed);
        }
    }
}