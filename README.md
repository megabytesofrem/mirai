# mirai
Server waifu.

## Usage
Run `npm run start:dev`. No idea how to bundle this for production atm lol.

## Features
- Custom event and command framework written from scratch
- Per command permission support
- A pretty help menu that lets you view specific commands help pages
- Basic moderation commands for kick/ban
- Filters for NSFW content/slurs
- Lots of shitpost/meme-y commands like `eggirl`, `pasta` and `reddit`

## Configuring Mirai
Currently, most of her is not configurable since she is custom built for my own private server. However, you can take a look at the `src/static` directory.

Eventually, in the long term I want to create a web based UI to configure her that you can self host and then edit her on a browser but thats for the future.